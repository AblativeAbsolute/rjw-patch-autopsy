﻿using HugsLib;

namespace RJW_patch_Autopsy
{
    public class Mod : ModBase
    {
        public override string ModIdentifier => "Stardust3D.RJW.patch.Autopsy";
    }
}